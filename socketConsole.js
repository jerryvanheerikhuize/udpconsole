#! /usr/bin/env node

/*#################################################################################*\
 
	 ██░ ██  ▄▄▄       ██▓      █████▒    ▄▄▄▄    ██▀███   ▒█████    ██████      
	▓██░ ██▒▒████▄    ▓██▒    ▓██   ▒    ▓█████▄ ▓██ ▒ ██▒▒██▒  ██▒▒██    ▒      
	▒██▀▀██░▒██  ▀█▄  ▒██░    ▒████ ░    ▒██▒ ▄██▓██ ░▄█ ▒▒██░  ██▒░ ▓██▄        
	░▓█ ░██ ░██▄▄▄▄██ ▒██░    ░▓█▒  ░    ▒██░█▀  ▒██▀▀█▄  ▒██   ██░  ▒   ██▒     
	░▓█▒░██▓ ▓█   ▓██▒░██████▒░▒█░       ░▓█  ▀█▓░██▓ ▒██▒░ ████▓▒░▒██████▒▒ ██▓ 
	 ▒ ░░▒░▒ ▒▒   ▓▒█░░ ▒░▓  ░ ▒ ░       ░▒▓███▀▒░ ▒▓ ░▒▓░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒▓▒ 
	 ▒ ░▒░ ░  ▒   ▒▒ ░░ ░ ▒  ░ ░         ▒░▒   ░   ░▒ ░ ▒░  ░ ▒ ▒░ ░ ░▒  ░ ░ ░▒  
	 ░  ░░ ░  ░   ▒     ░ ░    ░ ░        ░    ░   ░░   ░ ░ ░ ░ ▒  ░  ░  ░   ░   
	 ░  ░  ░      ░  ░    ░  ░            ░         ░         ░ ░        ░    ░  
											   ░                              ░ 
	-----------------------------------------------------------------------------

	Developed by:			Jerry van Heerikhuize
	Modified by:            Jerry van Heerikhuize

	-----------------------------------------------------------------------------

	Version:                1.0.0
	Creation Date:          28/04/18
	Modification Date:      28/04/18
	Email:                  hello@halfbros.nl
	Description:            create a socket CLI
	File:                   socketConsole.js

\*#################################################################################*/

const tools 	= require('./.bin/tools.js');
var print 		= tools.print;
var printError 	= tools.printError;
var printFatal 	= tools.printFatal;

const fs 		= require('fs');
const readline 	= require('readline'); 		// https://nodejs.org/api/readline.html
const dgram 	= require('dgram'); 		// https://nodejs.org/api/dgram.html

var configFile = './.socketConsolerc';

var configData,
	cliData,
	sdkData,
	scenariosData,
	CONFIG,
	CLI,
	SDK,
	scenarios;


console.log('\033c');
print(`SOCKETCONSOLE v1.0`, true, '\x1b[37m');

// Load config
try {
	print(`\nLoading configuration from ${configFile}`);
	configData = fs.readFileSync(configFile, 'utf8');
	CONFIG = JSON.parse(configData);
} catch (error) {
	printFatal(`Could not load the configuration file '${configFile}'. Make sure it exists and is a valid JSON file.`);
	process.exit(1); 
}

print(`Configuration from ${configFile} succesfully loaded`, false, '\x1b[32m');




// Load CLI model
try {
	print(`Loading CLI from ${CONFIG.cli}`);
	cliData = fs.readFileSync(CONFIG.cli, 'utf8');
	CLI = JSON.parse(cliData);
} catch (error) {
	printFatal(`Could not load the CLI file '${CONFIG.cli}'. Make sure it exists and is a valid JSON file.`);
	process.exit(1); 
}

print(`CLI from ${CONFIG.cli} succesfully loaded`, false, '\x1b[32m');




// Load SDK model
try {
	print(`Loading Scenarios from ${CONFIG.sdk}`);
	sdkData = fs.readFileSync(CONFIG.sdk, 'utf8');
	SDK = JSON.parse(sdkData);
} catch (error) {
	printFatal(`Could not load the SDK file '${CONFIG.sdk}'. Make sure it exists and is a valid JSON file.`);
	process.exit(1); 
}

print(`SDK from ${CONFIG.sdk} succesfully loaded`, false, '\x1b[32m');




// Load Scnenarios model
try {
	print(`Loading Scenarios from ${CONFIG.scenarios}`);
	scenariosData = fs.readFileSync(CONFIG.scenarios, 'utf8');
	scenarios = JSON.parse(scenariosData);
} catch (error) {
	printFatal(`Could not load the scenarios file '${CONFIG.scenarios}'. Make sure it exists and is a valid JSON file.`);
	process.exit(1); 
}

print(`Scenarios from ${CONFIG.scenarios} succesfully loaded`, false, '\x1b[32m');




// create console interface
global.rl = readline.createInterface(process.stdin, process.stdout);

rl.on('line', (stdin) => {

	var command = stdin.trim().split(" ")[0];

	if (CLI.includes(command)){

		switch(command) {
			case 'help':
			case '?':
				print (SDK);

				rl.setPrompt(`${CONFIG.promptprefix}: `, CONFIG.promptprefix.length + 2);
				rl.prompt();
				
				break;
			case 'quit':
			case 'q':
			case 'Q':
				rl.close();
				server.close();
				break;
			case 'Scenarios':
			case 'scn':
				print (scenarios);

				rl.setPrompt(`${CONFIG.promptprefix}: `, CONFIG.promptprefix.length + 2);
				rl.prompt();

				break;
		}

	} else if (SDK.includes(command)){
		print(`Sending '${stdin}' to ${CONFIG.client.host}:${CONFIG.client.port}` );

		server.send(stdin, 0, stdin.length, CONFIG.client.port, CONFIG.client.host, function(err, bytes) {
			if (err) {
				printError(`Unable to send command '${stdin}' to ${CONFIG.client.host}:${CONFIG.client.port}`, false, '\x1b[2m');
			}

			rl.setPrompt(`${CONFIG.promptprefix}: `, CONFIG.promptprefix.length + 2);
			rl.prompt();

		});
	
	} else if (scenarios.includes(command)){
		
		print(`Loading scenario file '${command}'`);

		rl.setPrompt(`${CONFIG.promptprefix}: `, CONFIG.promptprefix.length + 2);
		rl.prompt();

	} else {
		printError(`Command '${command}' was not found in the SDK. Please enter 'help' or '?' for SDK commands.`, false, '\x1b[2m');

		rl.setPrompt(`${CONFIG.promptprefix}: `, CONFIG.promptprefix.length + 2);
		rl.prompt();
	}

});




// create UDP server
try {
	print(`Creating an UDP server with protocol '${CONFIG.socket.protocol}' on port '${CONFIG.socket.port}'`);
	global.server = dgram.createSocket(CONFIG.socket.protocol);

	server.on('error', (err) => {
		console.log(`server error:\n${err.stack}`);
		server.close();
	});
	  
	server.on('message', (stdout, info) => {
		print(`received '${stdout}' from  client ${info.address}:${info.port}\n`, false, '\x1b[32m');
		rl.prompt();
	});
	  
	server.on('listening', () => {
		const address = server.address();

		print(`UDP server listening on ${address.address}:${address.port}\n`, false, '\x1b[32m');
		print(`${CONFIG.promptprefix}: welcome, please enter a command.` , true, '\x1b[37m');
		print('\nhelp || ? for help\nquit || q to quit\nscenarios || scn for scenarios\n', false, '\x1b[2m');

		rl.setPrompt(`${CONFIG.promptprefix}: `, CONFIG.promptprefix.length + 2);
		rl.prompt();
	});

	server.on('close', () => {
		print(`Quiting ${CONFIG.promptprefix}, see you next time :)`);
		print('\n\n\n');
		process.exit(0);
	});

	server.bind(CONFIG.socket.port, CONFIG.socket.host);

} catch (error) {
	printFatal(`An error occured creating an UDP server. Make sure your configuration is set properly.`);
	process.exit(1); 
}
