# Tello Node CLI
Interacting with the DJI/Ryze Tello using node.js.

1. Usage: node socketConsole

You should first connect the computer where you are running node.js to the Tello wireless network for your Tello.