#! /usr/bin/env node

const printFatal = function printError(str, isHead, color, reset) {
    color = (color === undefined) ? '\x1b[31m' : color;
    print(str, isHead, color, reset);
}

const printError = function printError(str, isHead, color, reset) {
    color = (color === undefined) ? '\x1b[31m' : color;
    print(str, isHead, color, reset);
}

const print = function print(str, isHead, color, reset) {
    const seperator = '---------------------------------------------------------------------------------';
    let suffix = '';

    isHead = (isHead === undefined) ? false : isHead;
    color = (color === undefined) ? '\x1b[37m' : color;
    suffix = (reset === true || reset === undefined) ? '\x1b[0m' : '';

    if (isHead) {
        console.log(`${seperator}\n`);
    }

    console.log(`${color}%s${suffix}`, str);

    if (isHead) {
        console.log(`\n${seperator}`);
    }
}

module.exports = {
    printFatal,
    printError,
    print
}
